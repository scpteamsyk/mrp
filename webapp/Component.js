sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"MRP/model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("MRP.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			var oRootPath = jQuery.sap.getModulePath("MRP"); // your resource root
		
			var oRootPathModel = new sap.ui.model.json.JSONModel({
				path : oRootPath
			});
		
			this.setModel(oRootPathModel, "rootPath");
		}
	});
});